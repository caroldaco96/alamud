
from .event import Event3

class PayWithEvent(Event3):
	NAME = "pay-with.have-paid"
	def perform(self):
		if self.object.has_prop("paid"):
			self.object.remove_prop("paid")
		if self.object2.id != "Argent-000":
			self.fail()
			return self.inform("pay-with.failed")
		self.inform("pay-with.have-paid")